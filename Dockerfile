FROM archlinux/base:latest

ADD files/ /

# Update
RUN pacman --noconfirm -Syu

# Install PHP
RUN pacman --noconfirm --needed -S php php-gd php-intl php-sodium php-sqlite

# Clean up
RUN rm -f \
      /var/cache/pacman/pkg/* \
      /var/lib/pacman/sync/* \
      /etc/pacman.d/mirrorlist.pacnew

